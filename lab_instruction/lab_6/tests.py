from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest

# Create your tests here.

class Lab4UnitTest(TestCase):
    def test_lab_4_table_url_exist(self):
        response = Client().get('/lab-6/')
        self.assertEqual(response.status_code, 200)

    def test_lab_4_table_using_message_table_func(self):
        found = resolve('/lab-4/result_table')
        self.assertEqual(found.func, message_table)

    def test_root_url_now_is_using_index_page_from_lab_4(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 301)
        self.assertRedirects(response,'/lab-4/',301,200)
