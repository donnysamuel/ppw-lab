# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-11-14 06:52
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lab_7', '0002_mahasiswa'),
    ]

    operations = [
        migrations.AlterField(
            model_name='friend',
            name='id',
            field=models.AutoField(primary_key=True, serialize=False),
        ),
    ]
